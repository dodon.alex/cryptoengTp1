#include<stdio.h>
#include<string.h>
#include <sys/random.h>
#include<aes-128_enc.c>

#define DISTINGUISHER_ARRAY_LEN 256
#define DISTINGUISHER_COUNT 16

void printBuffer(const uint8_t * buffer, int len) {
    for (int i = 0; i < len; i++) {
        printf("%02X", buffer[i]);
    }
    putchar('\n');
}

void testE1Q2() {
    const uint8_t prev_key[AES_128_KEY_SIZE] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};	

    printf("[E1Q2] Testing the implementation of prev_aes128_round_key.\n");

    uint8_t next_key[AES_128_KEY_SIZE];

    next_aes128_round_key(prev_key, next_key, 0);

    uint8_t toBeTested_prev_key[AES_128_KEY_SIZE];

    prev_aes128_round_key(next_key, toBeTested_prev_key, 0);

    printf("The next round key is:          ");
    printBuffer(next_key, AES_128_KEY_SIZE);
    printf("The prev round key is:          ");
    printBuffer(prev_key, AES_128_KEY_SIZE);
    printf("The computed prev round key is: ");
    printBuffer(toBeTested_prev_key, AES_128_KEY_SIZE);
}

void xorBuffers(uint8_t * dst, const uint8_t * src, int len) {
    for (int i = 0; i < len; i++) {
        dst[i] ^= src[i];
    }
}

void f(const uint8_t key1[AES_128_KEY_SIZE], const uint8_t key2[AES_128_KEY_SIZE], uint8_t message[AES_BLOCK_SIZE]) {
    uint8_t temp[AES_BLOCK_SIZE];
    memcpy(temp, message, AES_BLOCK_SIZE);
    aes128_enc(message, key1, 3, 1);
    aes128_enc(temp, key2, 3, 1);
    xorBuffers(message, temp, AES_BLOCK_SIZE);
}

void buildDistinguisher(uint8_t distinguisher[DISTINGUISHER_ARRAY_LEN][AES_BLOCK_SIZE]) {
    const uint8_t template[AES_BLOCK_SIZE];

    for (uint16_t i = 0; i < DISTINGUISHER_ARRAY_LEN; i++) {
        memcpy(distinguisher[i], template, AES_BLOCK_SIZE);
        distinguisher[i][0] = i;
    }
}

void testE1Q3() {
    const uint8_t k1[AES_128_KEY_SIZE] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    const uint8_t k2[AES_128_KEY_SIZE] = {0xd6, 0xaa, 0x74, 0xfd, 0xd2, 0xaf, 0x72, 0xfa, 0xda, 0xa6, 0x78, 0xf1, 0xd6, 0xab, 0x76, 0xfe};

    uint8_t message[AES_BLOCK_SIZE] = "Message SlugCDEF";
    uint8_t buffer[AES_BLOCK_SIZE];
    
    uint8_t distinguisher[DISTINGUISHER_ARRAY_LEN][AES_BLOCK_SIZE];
    uint8_t accumulator[AES_BLOCK_SIZE] = {0};
    
    printf("\n[E1Q3] Testing the implementation of the keyed function f.\n");
    printf("The two keys:\nk1: ");
    printBuffer(k1, AES_128_KEY_SIZE);
    printf("k2: ");
    printBuffer(k2, AES_128_KEY_SIZE);

    memcpy(buffer, message, AES_BLOCK_SIZE);
    printf("The message:                                ");
    printBuffer(message, AES_BLOCK_SIZE);
    printf("Applying F on the message with two keys k1: ");
    f(k1, k1, buffer);
    printBuffer(buffer, AES_BLOCK_SIZE);

    printf("Testing a distinguisher.\n");
    buildDistinguisher(distinguisher);
    
    for (int i = 0; i < DISTINGUISHER_ARRAY_LEN; i++) {
        f(k1, k2, distinguisher[i]);
        xorBuffers(accumulator, distinguisher[i], AES_BLOCK_SIZE);
    }

    printf("The sum of all the distinguisher cipher texts: ");
    printBuffer(accumulator, AES_BLOCK_SIZE);
}

uint8_t reverseByte(uint8_t toReverse, uint8_t keyByte) {
    uint8_t preAddKey = toReverse ^ keyByte;
    // we don't care to reverse the shift row step as the byte order doesn't matter for the attack
    return Sinv[preAddKey];
}

void accumulateKeyGuesses(
    uint8_t distinguisher[DISTINGUISHER_ARRAY_LEN][AES_BLOCK_SIZE], 
    long keyGuesses[AES_128_KEY_SIZE][UINT8_MAX + 1], 
    uint8_t isByteFound[AES_128_KEY_SIZE]
) {
    for (int i = 0; i < AES_128_KEY_SIZE; i++) {
        if(isByteFound[i]) {
            continue;
        }

        for (uint8_t lastGuess, guess = 0; lastGuess < guess || lastGuess == guess; lastGuess = guess, guess++) {
            uint8_t sum = 0;

            for (int dIndex = 0; dIndex < DISTINGUISHER_ARRAY_LEN; dIndex++) {
                sum ^= reverseByte(distinguisher[dIndex][i], guess);
            }

            if (sum == 0) {
                keyGuesses[i][guess]++;
            }
        }
    }
}

int foundKey(
    long keyGuesses[AES_128_KEY_SIZE][UINT8_MAX + 1], 
    uint8_t extractedKey[AES_128_KEY_SIZE], 
    uint8_t isByteFound[AES_128_KEY_SIZE]
) {
    int done = 1;
   
    for (int j, i = 0; i < AES_128_KEY_SIZE; i++) {
        int guessUnique = 0;
        int guessCount = -1;
        int guess = -1;

        for (j = 0; j < UINT8_MAX + 1; j++) {
            if(isByteFound[i]) {
                guessUnique = 0;
                continue;
            }
         
            if (keyGuesses[i][j] > 0) {
                if (guessCount == keyGuesses[i][j]) {
                    guessUnique = 0;
                } else if (keyGuesses[i][j] > guessCount) {
                    guessCount = keyGuesses[i][j];
                    guess = j;
                    guessUnique = 1;
                }
            } 
        }
        {
        printf("%d %d %d\n", i, guess, guessCount);
            for (int l = 0; l < UINT8_MAX + 1; l++) {
                printf("%d ", keyGuesses[i][l]);
            }
            putchar('\n');
        }
        if (guessUnique) {
            extractedKey[i] = guess;
            isByteFound[i] = 1;
            printf("Found unigue byte of key at index %d: %d\n", i, guess);
        } else {
            done = 0;
        }
    }

    return done;
}

void testE2Q1() {
    uint8_t key[AES_128_KEY_SIZE];
    uint8_t distinguisher[DISTINGUISHER_ARRAY_LEN][AES_BLOCK_SIZE];
    uint8_t accumulator[AES_BLOCK_SIZE] = { 0 };
    long keyGuesses[AES_128_KEY_SIZE][UINT8_MAX + 1] = { 0 };
    uint8_t extractedKey[AES_128_KEY_SIZE] = { 0 };
    uint8_t isByteFound[AES_128_KEY_SIZE] = { 0 };

    getrandom(key, AES_128_KEY_SIZE, 0);

    printf("\n[E2Q1] Attacking AES.\n");
    printf("Random key:    ");
    printBuffer(key, AES_128_KEY_SIZE);

    while(1) {
        buildDistinguisher(distinguisher);
        for (int i = 0; i < DISTINGUISHER_ARRAY_LEN; i++) {
            aes128_enc(distinguisher[i], key, 4, 0);
        }

        accumulateKeyGuesses(distinguisher, keyGuesses, isByteFound);

        if (foundKey(keyGuesses, extractedKey, isByteFound)) {
            break;
        }
    }

    printf("Extracted key: ");
    printBuffer(extractedKey, AES_128_KEY_SIZE);
}

int main() {
    testE1Q2();
    testE1Q3();
    testE2Q1();

    return 0;
}